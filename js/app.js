import {getAllComputers} from '../api/computersApi.js';
import {
    selectComputer,
    setPictureInfo,
    setDescriptionAndPrice
} from './computerHelper.js';

class App {
    constructor() {
        // HTML Elements
        this.elBankBtn = document.getElementById('bank-btn');
        this.elPay = document.getElementById('pay'); 
        this.elBalance = document.getElementById('balance');
        this.elLoan = document.getElementById('loan');
        this.elLoanAmount = document.getElementById('loan-amount');
        this.elComputersSelect = document.getElementById('computers-select');
        this.elComputersFeatureList = document.getElementById('computers-feature-list');
        this.elComputerPicture = document.getElementById('computer-picture');
        this.elComputerDescription = document.getElementById('computer-name');
        this.elComputerPrice = document.getElementById('computer-price');
        this.elComputerDescriptionParagraf = document.getElementById('computer-description-paragraf');
        this.elComputerBuy = document.getElementById('computer-buy');
        this.elWorkBtn = document.getElementById('work-btn');

        // Properties
        this.error = '';
        this.computerList = [];

        return this;
    }

    async init() {
        this.bindEvents();
        await this.loadComputers();
    }

    //Get the computer data
    async loadComputers() {
        try {
            this.computerList = await getAllComputers(); 
            console.log()
        } catch (error) {
            this.error = error.message;
        } finally {
            this.loadComputerData(this);
        }
    }
    
    //Adds the computer options to the select box and fills information about the first elem
    loadComputerData() {
        this.computerList.forEach(element => {
            let opt = document.createElement("option");
            opt.text = `${element.name}`;
            opt.value = `${element.id}`;
            this.elComputersSelect.options.add(opt);
        });
        this.setComputerInfo(this.computerList[0]);
    }
    
    //Sets the information about the chosen computer where it should be
    setComputerInfo(computer) {
        setPictureInfo(computer, this.elComputerPicture);
        setDescriptionAndPrice(computer, this.elComputerDescription, this.elComputerDescriptionParagraf, this.elComputerPrice, this.elComputersFeatureList);
    }
    
    bindEvents() {
        this.elComputerBuy.addEventListener('click', () =>{
            //Check bank balance to see if the person has enough cash        
            let bankBalance = parseInt(this.elBalance.innerText);
            let computerPrice = parseInt(this.elComputerPrice.innerText);
    
            if(computerPrice > bankBalance)
            {
                alert("You don't have enough cash to buy this item");
                return;
            }
            //Remove money from the bank
            this.elBalance.innerText = bankBalance - computerPrice;
            this.elLoanAmount.innerText = 0;
            
            //alert that the person now owns the computer
            alert(`Congratulations! You now own a ${this.elComputerDescription.innerText}`);
        })

        //Get a loan
        this.elLoan.addEventListener('click', () => {
            let loanAmount = parseInt(this.elLoanAmount.innerText);
            if(loanAmount)
            {
                alert("Cannot get more than one bank loan before buying a computer");
                return;
            }
            
            //Read input from the user
            let response = prompt("How much loan do you want?");
            let amount = parseInt(response);
            
            //Check if it could parse the input
            if(!amount)
            {
                alert("Must be a number");
                return;
            } 
            let balance = parseInt(this.elBalance.innerText);
            
            if(amount > balance*2)
            {
                alert("Cannot loan an amount bigger than double of the balance");
                return;
            }
            
            //Set the new balance with the loaned money
            this.elBalance.innerText = balance + amount;
            this.elLoanAmount.innerText = amount;
        })
        //New computer selected
        this.elComputersSelect.addEventListener('input', (event) => {
            let selectedComputer = selectComputer(event.target.value, this.computerList);
            this.setComputerInfo(selectedComputer);
        })
        
        // When one click the Work button must add 100 to the pay
        this.elWorkBtn.addEventListener('click', () => {
            this.elPay.innerText = parseInt(this.elPay.innerText)+100;
        })
        
        // When one click the Bank button adds the pay to the balance in bank
        this.elBankBtn.addEventListener('click', () => {
            //set new balance
            this.elBalance.innerText = parseInt(this.elBalance.innerText) + parseInt(this.elPay.innerText);
            
            //Clear the work
            this.elPay.innerText = 0;
        })
    }

}

new App().init();