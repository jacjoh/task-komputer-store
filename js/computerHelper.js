
//Select a single computer
export function selectComputer(id, computerList) {
    return computerList.find(element => element.id == id);
}

//add the feature list for the computer to the html
function setFeatureList(computer, elComputersFeatureList) {
    elComputersFeatureList.innerHTML = computer.featureList;
}

//Adds the computer picture to the html
export function setPictureInfo(computer, elComputerPicture) {
    elComputerPicture.innerHTML = "";
    let elem = document.createElement("img");
    elem.setAttribute("src", `${computer.imageLink}`);
    elem.setAttribute("width", "100%");
    elem.setAttribute("alt", "Flower");
    elComputerPicture.appendChild(elem);
}

//Adds the computer description and price to the html
export function setDescriptionAndPrice(computer, elComputerDescription, elComputerDescriptionParagraf, elComputerPrice, elComputersFeatureList) {
    setFeatureList(computer, elComputersFeatureList);
    elComputerDescription.innerText = computer.name;
    elComputerDescriptionParagraf.innerText = computer.description;
    elComputerPrice.innerText = computer.price;
}
