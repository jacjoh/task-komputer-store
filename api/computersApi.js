
// Use the Fetch API & ES6
export const getAllComputers = () => {
    return fetch('../computers.json')
        .then(response => response.json())
        .then(data => data)
    };
