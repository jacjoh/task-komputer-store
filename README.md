# Task Komputer Store

Computer store web application where one can buy computers.

#How it works
In the application you can work to get money, and place the money in the bank. For each time you press work you get 100kr. 

You can also get a loan maximum twice the amount of money you have in the bank. You are only able to get one loan before buying a computer. 

There are 4 different computers you can choose between, and by selecting a computer you get information about the computer displayed on the screen.

The information about the computers are stored in a JSON file, and at the start of the program this file is read to get the data about the different computers.